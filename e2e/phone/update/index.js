import { assert } from 'chai';

import { req } from '../../../src/common/utils';
import ENDPOINTS from '../../../src/common/endpoints';
import logger from '../../../src/service/winston';
import { ERROR_INVALID_INPUT, ERROR_PHONE_ALREADY_EXIST, ERROR_PHONE_NOT_FOUND } from '../../../src/common/errors';

export const updatePhoneWithInvalidInput = async () => {
  logger.debug('Try to update phone with invalid input...');
  let result = await req('post',
    ENDPOINTS.phone.path + ENDPOINTS.phone.endpoints.postUpdateOneByOID.path
      .replace(':phoneOID', '5dc579b978a9c82368cb8f05'),
    {
      phoneNumber: '+32 47 2703316',
      firstName: 'Nolan',
    });
  assert.deepEqual(result, ERROR_INVALID_INPUT);
  result = await req('post',
    ENDPOINTS.phone.path + ENDPOINTS.phone.endpoints.postUpdateOneByOID.path
      .replace(':phoneOID', '5dc579b978a9c82368cb8f05'),
    {
      phoneNumber: '+32 47 2703316',
      lastName: 'Vanmoortel',
    });
  assert.deepEqual(result, ERROR_INVALID_INPUT);
  result = await req('post',
    ENDPOINTS.phone.path + ENDPOINTS.phone.endpoints.postUpdateOneByOID.path
      .replace(':phoneOID', '5dc579b978a9c82368cb8f05'),
    {
      firstName: 'Nolan',
      lastName: 'Vanmoortel',
    });
  assert.deepEqual(result, ERROR_INVALID_INPUT);
  result = await req('post',
    ENDPOINTS.phone.path + ENDPOINTS.phone.endpoints.postUpdateOneByOID.path
      .replace(':phoneOID', '5dc579b978a9c82368cb8f05'),
    {
      phoneNumber: '+32 472703316',
      firstName: 'Nolan',
      lastName: 'Vanmoortel',
    });
  assert.deepEqual(result, ERROR_INVALID_INPUT);
  result = await req('post',
    ENDPOINTS.phone.path + ENDPOINTS.phone.endpoints.postUpdateOneByOID.path
      .replace(':phoneOID', '5dc579b978a9c82368cb8f05'),
    {
      phoneNumber: '32 47 2703316',
      firstName: 'Nolan',
      lastName: 'Vanmoortel',
    });
  assert.deepEqual(result, ERROR_INVALID_INPUT);
  result = await req('post',
    ENDPOINTS.phone.path + ENDPOINTS.phone.endpoints.postUpdateOneByOID.path
      .replace(':phoneOID', '5dc579b978a9c82368cb8f05'),
    {
      phoneNumber: '+32 47 2703',
      firstName: 'Nolan',
      lastName: 'Vanmoortel',
    });
  assert.deepEqual(result, ERROR_INVALID_INPUT);
  result = await req('post',
    ENDPOINTS.phone.path + ENDPOINTS.phone.endpoints.postUpdateOneByOID.path
      .replace(':phoneOID', '5dc579b978a9c82368cb8f05'),
    {
      phoneNumber: '+32 47 2703316',
      firstName: 'Nolan',
      lastName: 'Vanmoortel',
    });
  assert.deepEqual(result, ERROR_PHONE_NOT_FOUND);
  logger.debug('Try to update phone with invalid input done.');
};

export const updatePhoneWithAlreadyUsedPhoneNumber = async (phoneNumber, phoneOID) => {
  logger.debug('Try to update phone with already used phone number...');
  const result = await req('post',
    ENDPOINTS.phone.path + ENDPOINTS.phone.endpoints.postUpdateOneByOID.path
      .replace(':phoneOID', phoneOID),
    {
      phoneNumber,
      firstName: 'Nolan',
      lastName: 'Vanmoortel',
    });
  assert.deepEqual(result, ERROR_PHONE_ALREADY_EXIST);
  logger.debug('Try to update phone with already used phone number done.');

  return result.data;
};

export const updatePhone = async (phone) => {
  logger.debug('Try to update phone...');
  const result = await req('post',
    ENDPOINTS.phone.path + ENDPOINTS.phone.endpoints.postUpdateOneByOID.path
      .replace(':phoneOID', phone.oid),
    {
      phoneNumber: phone.phoneNumber,
      firstName: phone.firstName,
      lastName: phone.lastName,
    });
  assert.exists(result.data);
  assert.isOk(result.data.oid);
  assert.equal(result.data.phoneNumber, phone.phoneNumber);
  assert.equal(result.data.lastName, phone.lastName);
  assert.equal(result.data.firstName, phone.firstName);
  logger.debug('Try to update phone done.');

  return result.data;
};
