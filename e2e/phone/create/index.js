import { assert } from 'chai';

import { req } from '../../../src/common/utils';
import ENDPOINTS from '../../../src/common/endpoints';
import logger from '../../../src/service/winston';
import { ERROR_INVALID_INPUT, ERROR_PHONE_ALREADY_EXIST } from '../../../src/common/errors';

export const createPhoneWithInvalidInput = async () => {
  logger.debug('Try to create phone with invalid input...');
  let result = await req('put',
    ENDPOINTS.phone.path + ENDPOINTS.phone.endpoints.putCreatePhone.path,
    {
      phoneNumber: '+32 47 2703316',
      firstName: 'Nolan',
      lastName: '',
    });
  assert.deepEqual(result, ERROR_INVALID_INPUT);
  result = await req('put',
    ENDPOINTS.phone.path + ENDPOINTS.phone.endpoints.putCreatePhone.path,
    {
      phoneNumber: '+32 47 2703316',
      lastName: 'Vanmoortel',
    });
  assert.deepEqual(result, ERROR_INVALID_INPUT);
  result = await req('put',
    ENDPOINTS.phone.path + ENDPOINTS.phone.endpoints.putCreatePhone.path,
    {
      firstName: 'Nolan',
      lastName: 'Vanmoortel',
    });
  assert.deepEqual(result, ERROR_INVALID_INPUT);
  result = await req('put',
    ENDPOINTS.phone.path + ENDPOINTS.phone.endpoints.putCreatePhone.path,
    {
      phoneNumber: '32 47 2703316',
      firstName: 'Nolan',
      lastName: 'Vanmoortel',
    });
  assert.deepEqual(result, ERROR_INVALID_INPUT);
  result = await req('put',
    ENDPOINTS.phone.path + ENDPOINTS.phone.endpoints.putCreatePhone.path,
    {
      phoneNumber: '+32 472703316',
      firstName: 'Nolan',
      lastName: 'Vanmoortel',
    });
  assert.deepEqual(result, ERROR_INVALID_INPUT);
  result = await req('put',
    ENDPOINTS.phone.path + ENDPOINTS.phone.endpoints.putCreatePhone.path,
    {
      phoneNumber: '+32 47 2703',
      firstName: 'Nolan',
      lastName: 'Vanmoortel',
    });
  assert.deepEqual(result, ERROR_INVALID_INPUT);
  logger.debug('Try to create phone with invalid input done.');
};

export const createPhoneWithAlreadyUsedPhoneNumber = async (phoneNumber) => {
  logger.debug('Try to create phone with already used phone number...');
  const result = await req('put',
    ENDPOINTS.phone.path + ENDPOINTS.phone.endpoints.putCreatePhone.path,
    { phoneNumber, firstName: 'John', lastName: 'Doe' });
  assert.deepEqual(result, ERROR_PHONE_ALREADY_EXIST);
  logger.debug('Try to create phone with already used phone number done.');

  return result.data;
};

export const createPhone = async (phone) => {
  logger.debug('Try to create phone...');
  const result = await req('put',
    ENDPOINTS.phone.path + ENDPOINTS.phone.endpoints.putCreatePhone.path,
    phone);
  assert.exists(result.data);
  assert.isOk(result.data.oid);
  assert.equal(result.data.phoneNumber, phone.phoneNumber);
  assert.equal(result.data.lastName, phone.lastName);
  assert.equal(result.data.firstName, phone.firstName);
  logger.debug('Try to create phone done.');

  return result.data;
};
