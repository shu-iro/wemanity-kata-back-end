import { assert } from 'chai';

import { req } from '../../../src/common/utils';
import ENDPOINTS from '../../../src/common/endpoints';
import logger from '../../../src/service/winston';
import { ERROR_INVALID_INPUT } from '../../../src/common/errors';

export const readPhoneWithInvalidInput = async () => {
  logger.debug('Try to read all phone with invalid input...');
  let result = await req('get',
    `${ENDPOINTS.phone.path + ENDPOINTS.phone.endpoints.getRead.path}?offset=azerty`);
  assert.deepEqual(result, ERROR_INVALID_INPUT);
  result = await req('get',
    `${ENDPOINTS.phone.path + ENDPOINTS.phone.endpoints.getRead.path}?limit=azerty`);
  assert.deepEqual(result, ERROR_INVALID_INPUT);
  result = await req('get',
    `${ENDPOINTS.phone.path + ENDPOINTS.phone.endpoints.getRead.path}?offset=-1`);
  assert.deepEqual(result, ERROR_INVALID_INPUT);
  result = await req('get',
    `${ENDPOINTS.phone.path + ENDPOINTS.phone.endpoints.getRead.path}?limit=0`);
  assert.deepEqual(result, ERROR_INVALID_INPUT);
  result = await req('get',
    `${ENDPOINTS.phone.path + ENDPOINTS.phone.endpoints.getRead.path}?offset=6&limit=5`);
  assert.deepEqual(result, ERROR_INVALID_INPUT);
  result = await req('get',
    `${ENDPOINTS.phone.path + ENDPOINTS.phone.endpoints.getRead.path}?offset=0&limit=10100`);
  assert.deepEqual(result, ERROR_INVALID_INPUT);
  logger.debug('Try to read all phone with invalid input done.');
};

export const readPhone = async () => {
  logger.debug('Try to read all phone...');
  const result = await req('get',
    ENDPOINTS.phone.path + ENDPOINTS.phone.endpoints.getRead.path);
  assert.exists(result.data);
  logger.debug('Try to read all phone done.');

  return result.data;
};
