import { assert } from 'chai';

import logger from '../../../src/service/winston';
import { createPhone, createPhoneWithAlreadyUsedPhoneNumber, createPhoneWithInvalidInput } from '../../phone/create';
import { readPhone, readPhoneWithInvalidInput } from '../../phone/read';
import { updatePhone, updatePhoneWithAlreadyUsedPhoneNumber, updatePhoneWithInvalidInput } from '../../phone/update';

export default async () => {
  logger.info('############################################');
  logger.info('###### Try create/read/update phonebook entry with invalid input...');

  await createPhoneWithInvalidInput();
  await readPhoneWithInvalidInput();
  await updatePhoneWithInvalidInput();

  logger.info('###### Try create/read/update phonebook entry with invalid input DONE.');
  logger.info('############################################');


  logger.info('############################################');
  logger.info('###### Try to create, retrieve and update one phonebook entry ...');

  logger.debug('Check retrieve empty phonebook...');
  const phoneListEmpty = await readPhone();
  assert.equal(phoneListEmpty.length, 0);
  logger.debug('Check retrieve empty phonebook done.');

  logger.debug('Try to create one phone book entry...');
  const phone1 = await createPhone({
    phoneNumber: '+39 02 1234567',
    firstName: 'John',
    lastName: 'Doe',
  });
  const phoneList1 = await readPhone();
  assert.equal(phoneList1.length, 1);
  assert.deepEqual(phoneList1[0], phone1);
  logger.debug('Try to create one phone book entry done.');

  logger.debug('Try to update one phone book entry...');
  const phoneUpdate1 = await updatePhone({
    oid: phone1.oid,
    phoneNumber: '+32 47 2703316',
    firstName: 'Nolan',
    lastName: 'Vanmoortel',
  });
  const phoneList1Updated = await readPhone();
  assert.equal(phoneList1Updated.length, 1);
  assert.deepEqual(phoneList1Updated[0], phoneUpdate1);
  logger.debug('Try to update one phone book entry done.');

  logger.info('###### Try to create, retrieve and update one phonebook entry DONE.');
  logger.info('############################################');


  logger.info('############################################');
  logger.info('###### Try create/update phonebook with phone number already used...');

  logger.debug('Create new one to check the unicity of the phone number...');
  const phone2 = await createPhone({
    phoneNumber: '+39 02 1234567',
    firstName: 'John',
    lastName: 'Doe',
  });
  const phoneList2 = await readPhone();
  assert.equal(phoneList2.length, 2);
  assert.deepEqual(phoneList2[0], phoneUpdate1);
  assert.deepEqual(phoneList2[1], phone2);
  logger.debug('Create new one to check the unicity of the phone number DONE.');

  await createPhoneWithAlreadyUsedPhoneNumber(phoneUpdate1.phoneNumber);
  await updatePhoneWithAlreadyUsedPhoneNumber(phone2.phoneNumber, phoneUpdate1.oid);

  logger.info('###### Try create/update phonebook with phone number already used DONE.');
  logger.info('############################################');
};
