import { assert } from 'chai';

import logger from '../src/service/winston';
import { req } from '../src/common/utils';
import ENDPOINTS from '../src/common/endpoints';
// eslint-disable-next-line no-unused-vars
import serverAPI from '../src'; // Start the API
import simplePhoneOperation from './scenario/simplePhoneOperation';

const main = async () => {
  try {
    logger.info('Start test end-to-end...');

    logger.debug('Start the API...');
    logger.debug('Wait 5 sec...');
    await new Promise((res) => setTimeout(res, 5000));

    logger.debug('Try to contact the API...');
    const res = await req('get', `${ENDPOINTS.alive.path}${ENDPOINTS.alive.endpoints.getAlive.path}`);
    assert.exists(res.data, 'Try to contact the API failed.');
    logger.debug('Try to contact the API done.');
    logger.debug('Start the API done.');

    await simplePhoneOperation();

    logger.info('Start test end-to-end done.');
    process.exit(0);
  } catch (err) {
    logger.error(err.stack);
    process.exit(1);
  }
};

main();
