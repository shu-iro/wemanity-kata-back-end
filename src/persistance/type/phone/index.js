// @flow
import { ObjectID } from 'mongodb';

export type PhoneMongoDB = {
  _id: ObjectID,
  phoneNumber: string,
  firstName: string,
  lastName: string,
};
