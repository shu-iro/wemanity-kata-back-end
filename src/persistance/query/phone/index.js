// @flow
import { Db, ObjectID } from 'mongodb';

import type { Response } from '../../../business/type/response';
import type { Phone } from '../../../business/type/phone';
import type { PhoneMongoDB } from '../../type/phone';

import logger from '../../../service/winston';
import { ERROR_PHONE_CREATE, ERROR_PHONE_NOT_FOUND, ERROR_PHONE_QUERY_UNEXPECTED } from '../../../common/errors';
import { makePhoneFromPhoneMongoDB, makePhoneMongoDBFromPhone } from '../../../business/factory/phone';

const collection = 'phone';

export const createPhoneNumberIndex = async (db: Db) => db.collection(collection)
  .createIndex({ phoneNumber: 1 }, { unique: true });

export const create = async (phone: Phone, db: Db): Promise<Response<Phone>> => {
  try {
    const { insertedId } = await db.collection(collection)
      .insertOne(makePhoneMongoDBFromPhone(phone));

    if (!insertedId) return ERROR_PHONE_CREATE;

    return { status: 200, data: { ...phone, oid: insertedId.toString() } };
  } catch (err) {
    logger.error(err.stack);
    return ERROR_PHONE_QUERY_UNEXPECTED;
  }
};

const read = async (
  offset: number = 0,
  limit: number = 9999,
  filter: Object,
  db: Db,
): Promise<Response<Phone[]>> => {
  try {
    const phoneMongoDB: PhoneMongoDB[] = await db.collection(collection)
      .find(filter)
      .skip(offset)
      .limit(limit)
      .toArray();

    return {
      status: 200,
      data: phoneMongoDB.map((p) => makePhoneFromPhoneMongoDB(p)),
    };
  } catch (err) {
    logger.error(err.stack);
    return ERROR_PHONE_QUERY_UNEXPECTED;
  }
};

export const readAll = async (
  offset: number = 0,
  limit: number = 9999,
  db: Db,
): Promise<Response<Phone[]>> => read(offset, limit, {}, db);

const readOne = async (
  filter: Object,
  db: Db,
): Promise<Response<Phone>> => {
  try {
    const phoneMongoDB: PhoneMongoDB = await db.collection(collection)
      .findOne(filter);

    if (!phoneMongoDB) return ERROR_PHONE_NOT_FOUND;

    return { status: 200, data: makePhoneFromPhoneMongoDB(phoneMongoDB) };
  } catch (err) {
    logger.error(err.stack);
    return ERROR_PHONE_QUERY_UNEXPECTED;
  }
};

export const readOneByPhoneNumber = async (
  phoneNumber: string,
  db: Db,
): Promise<Response<Phone>> => readOne({ phoneNumber }, db);

export const readOneByOID = async (
  phoneOID: string,
  db: Db,
): Promise<Response<Phone>> => readOne({ _id: ObjectID(phoneOID) }, db);

const updateOne = async (
  filter: Object,
  updateOp: Object,
  db: Db,
): Promise<Response<Phone>> => {
  try {
    const result: { value: PhoneMongoDB } = await db.collection(collection)
      .findOneAndUpdate(
        filter, // Only update if version match (race condition)
        updateOp,
        { returnOriginal: false },
      );

    if (!result.value) return ERROR_PHONE_NOT_FOUND;

    return { status: 200, data: makePhoneFromPhoneMongoDB(result.value) };
  } catch (err) {
    logger.error(err.stack);
    return ERROR_PHONE_QUERY_UNEXPECTED;
  }
};

export const updateOneByOID = async (
  phone: Phone,
  db: Db,
): Promise<Response<Phone>> => updateOne(
  { _id: ObjectID(phone.oid) },
  {
    $set: {
      phoneNumber: phone.phoneNumber,
      firstName: phone.firstName,
      lastName: phone.lastName,
    },
  },
  db,
);
