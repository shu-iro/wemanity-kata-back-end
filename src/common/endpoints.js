const ENDPOINTS = {
  alive: {
    path: '/alive',
    endpoints: {
      getAlive: {
        path: '/',
      },
    },
  },
  phone: {
    path: '/phone',
    endpoints: {
      putCreatePhone: {
        path: '',
      },
      getRead: {
        path: '',
      },
      postUpdateOneByOID: {
        path: '/:phoneOID',
      },
    },
  },
};

export default ENDPOINTS;
