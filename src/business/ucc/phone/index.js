// @flow
import { Db } from 'mongodb';

import type { Response } from '../../type/response';
import type { Phone } from '../../type/phone';

import logger from '../../../service/winston';
import {
  ERROR_INVALID_INPUT,
  ERROR_PHONE_ALREADY_EXIST,
  ERROR_PHONE_NOT_FOUND,
  ERROR_PHONE_UNEXPECTED,
} from '../../../common/errors';
import { makePhone } from '../../factory/phone';
import {
  readOneByPhoneNumber as readOneByPhoneNumberDB,
  create as createPhoneDB, readAll as readAllPhoneDB,
  readOneByOID as readOnePhoneByOIDDB, updateOneByOID as updateOnePhoneByOIDDB,
} from '../../../persistance/query/phone';

export const checkPhoneNumber = (
  phoneNumber: string,
): boolean => /^([+][0-9]+[\s][0-9]+[\s][0-9]{6,})$/.test(phoneNumber);

/*
 *
 *  PUT /phone Create a phonebook entry
 *
 */
export const create = async (
  body: { phoneNumber: string, firstName: string, lastName: string },
  db: Db,
): Promise<Response<Phone>> => {
  try {
    logger.debug('Check body...');
    if (!body.firstName || !body.lastName
      || !body.phoneNumber || !checkPhoneNumber(body.phoneNumber)) return ERROR_INVALID_INPUT;
    logger.debug('Check body done.');

    logger.debug('Make phone...');
    const phone: Phone = makePhone(body.phoneNumber, body.firstName, body.lastName);
    logger.debug('Make phone done.');

    logger.debug('Check if phone number already exist...');
    const resultExist: Response<Phone> = await readOneByPhoneNumberDB(phone.phoneNumber, db);
    if (resultExist.data) return ERROR_PHONE_ALREADY_EXIST;
    logger.debug('Check if phone number already exist done.');

    logger.debug('Persist phone...');
    const resultCreate: Response<Phone> = await createPhoneDB(phone, db);
    if (!resultCreate.data) return resultCreate;
    logger.debug('Persist phone done.');

    return resultCreate;
  } catch (err) {
    logger.error(err.stack);
    return ERROR_PHONE_UNEXPECTED;
  }
};

/*
 *
 *  GET /phone Read all phone entry
 *  Max 10 000 return
 *
 */
export const readAll = async (
  _offset: string = '0',
  _limit: string = '9999',
  db: Db,
): Promise<Response<Phone[]>> => {
  try {
    logger.debug('Check body...');
    const offset: number = parseInt(_offset, 10);
    const limit: number = parseInt(_limit, 10);
    // eslint-disable-next-line no-restricted-globals
    if (isNaN(offset) || isNaN(limit)
      || limit - offset > 10000 || limit - offset < 0
      || limit < 1 || offset < 0) return ERROR_INVALID_INPUT;
    logger.debug('Check body done.');

    logger.debug('Retrieve all phone...');
    const resultRead: Response<Phone[]> = await readAllPhoneDB(offset, limit, db);
    if (!resultRead.data) return resultRead;
    logger.debug('Retrieve all phone done.');

    return resultRead;
  } catch (err) {
    logger.error(err.stack);
    return ERROR_PHONE_UNEXPECTED;
  }
};

/*
 *
 *  POST /phone Update a phonebook entry
 *  Need to provide all phone field.
 *  TODO If API is used by other API maybe implement more easy todo partially update
 *
 */
export const updateOneByOID = async (
  phoneOID: string,
  body: { phoneNumber: string, firstName: string, lastName: string },
  db: Db,
): Promise<Response<Phone>> => {
  try {
    logger.debug('Check body...');
    if (!phoneOID || !body.firstName || !body.lastName
      || !body.phoneNumber || !checkPhoneNumber(body.phoneNumber)) return ERROR_INVALID_INPUT;
    logger.debug('Check body done.');

    logger.debug('Retrieve phone number by OID...');
    const resultOld: Response<Phone> = await readOnePhoneByOIDDB(phoneOID, db);
    if (!resultOld.data) return ERROR_PHONE_NOT_FOUND;
    logger.debug('Retrieve phone number by OID done.');

    logger.debug('Make phone...');
    const phoneUpdated: Phone = makePhone(body.phoneNumber, body.firstName,
      body.lastName, phoneOID);
    logger.debug('Make phone done.');

    if (phoneUpdated.phoneNumber !== resultOld.data.phoneNumber) {
      logger.debug('Check if new phone number already exist...');
      const resultExist: Response<Phone> = await readOneByPhoneNumberDB(
        phoneUpdated.phoneNumber,
        db,
      );
      if (resultExist.data) return ERROR_PHONE_ALREADY_EXIST;
      logger.debug('Check if new phone number already exist done.');
    }

    logger.debug('Persist updated phone...');
    const resultUpdate: Response<Phone> = await updateOnePhoneByOIDDB(phoneUpdated, db);
    if (!resultUpdate.data) return resultUpdate;
    logger.debug('Persist updated phone done.');

    return resultUpdate;
  } catch (err) {
    logger.error(err.stack);
    return ERROR_PHONE_UNEXPECTED;
  }
};
