// @flow

export type Phone = {
  oid: string,
  phoneNumber: string,
  firstName: string,
  lastName: string,
};
