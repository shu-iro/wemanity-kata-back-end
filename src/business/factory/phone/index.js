// @flow
import { ObjectID } from 'mongodb';

import type { Phone } from '../../type/phone';
import type { PhoneMongoDB } from '../../../persistance/type/phone';

export const makePhone = (
  phoneNumber: string,
  firstName: string,
  lastName: string,
  oid: string = '',
): Phone => ({
  oid,
  phoneNumber,
  firstName,
  lastName,
});

export const makePhoneMongoDBFromPhone = (phone: Phone): PhoneMongoDB => ({
  _id: phone.oid ? ObjectID(phone.oid) : undefined,
  phoneNumber: phone.phoneNumber,
  firstName: phone.firstName,
  lastName: phone.lastName,
});

export const makePhoneFromPhoneMongoDB = (phoneMongoDB: PhoneMongoDB): Phone => ({
  // eslint-disable-next-line no-underscore-dangle
  oid: phoneMongoDB._id.toString(),
  phoneNumber: phoneMongoDB.phoneNumber,
  firstName: phoneMongoDB.firstName,
  lastName: phoneMongoDB.lastName,
});
