import ENDPOINTS from '../common/endpoints';
import alive from './alive';
import phone from './phone';

const loadRouters = (app) => {
  app.use(ENDPOINTS.alive.path, alive);
  app.use(ENDPOINTS.phone.path, phone);
};

export default loadRouters;
