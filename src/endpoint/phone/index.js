import { Router } from 'express';
import ENDPOINTS from '../../common/endpoints';
import { answer } from '../../service/winston';
import { create, readAll, updateOneByOID } from '../../business/ucc/phone';

const { endpoints } = ENDPOINTS.phone;
const router = Router();

router.put(endpoints.putCreatePhone.path, async (req, res) => {
  const result = await create(req.body, req.app.locals.db);
  return answer(result.status, result, req, res);
});

router.get(endpoints.getRead.path, async (req, res) => {
  const result = await readAll(req.query.offset, req.query.limit, req.app.locals.db);
  return answer(result.status, result, req, res);
});

router.post(endpoints.postUpdateOneByOID.path, async (req, res) => {
  const result = await updateOneByOID(req.params.phoneOID, req.body, req.app.locals.db);
  return answer(result.status, result, req, res);
});

export default router;
